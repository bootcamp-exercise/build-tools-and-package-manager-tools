Your team wants to build out a small helper library in Java and ask you to take over the project.


**EXERCISE 0: Clone project and create own Git repository**

Clone the project and
create your own project/git repository from it

**EXERCISE 1: Build jar artifact**

You want to deploy the artifact to share that library with all team members. So try to build the jar file
The Build will fail, because of a compile error in a test, so you can't build the jar.


**EXERCISE 2: Run tests**

Fix the test, by changing "true" string to true boolean.
Run gradle test to execute only the tests and check the fix.


**EXERCISE 3: Clean and build App**

You fixed the test. Now:
clean the build folder
try to build jar file again.


**EXERCISE 4: Start application**

Start the jar file to test that the application runs successfully as a jar file


**EXERCISE 5: Start App with 2 Parameters**

Now you want to add parameters to your application, so you and other users can pass different values on startup.
Rebuild the jar file
Execute the jar file again with 2 params
