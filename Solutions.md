<details>
<summary>Exercise 0: Clone project and create own Git repository </summary>
 <br />

```sh
# clone repository & change into project dir
git clone git@gitlab.com:twn-devops-bootcamp/latest/04-build-tools/build-tools-exercises.git
cd build-tools-exercises-project

# remove remote reference and add your project repository
rm -rf .git
git init 
git add .
git commit -m "Initial commit"
git remote add origin 
git push -u origin master

```

</details>

******
<details>
<summary>Exercise 1: Build jar artifact </summary>
 <br />


```sh
gradle build

```

</details>

******
<details>
<summary>Exercise 2: Run tests </summary>
 <br />

```sh
# change the condition to true
boolean result = myApp.getCondition(true); 

# run tests
gradle test

```

</details>

******

<details>
<summary>Exercise 3: Clean & build App </summary>
 <br />


```sh
gradle clean 
gradle build

```

</details>

******

<details>
<summary>Exercise 4: Start application </summary>
 <br />


```sh
#cd to the path where the jar file is created
java -jar build-tools-exercises-1.0-SNAPSHOT.jar

```

</details>

******

<details>
<summary>Exercise 5: Start App with 2 Parameters </summary>
 <br />

```sh
# added this code snippet in application file
Logger log = LoggerFactory.getLogger(Application.class); 
try { 
    String one = args[0]; 
    String two = args[1]; 
    log.info("Application will start with the parameters {} and {}", one, two); 
} catch (Exception e) { 
    log.info("No parameters provided"); 
}


gradle build
# run application with two parameters
java -jar build-tools-exercises-1.0-SNAPSHOT.jar Hello There

```

</details>

